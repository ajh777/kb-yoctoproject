Useful YP basics to beginners, see wiki pages.

A BitBake style HelloWorld example of building nano editor, from chapter 4.6 of the book "Embedded Linux Systems with the Yocto Project", with few modifications
1. Setup bitbake build env first
2. Edit build/conf/bblayers.conf, add a new layer path into BBLAYERS e.g. "/path/to/project/meta-hello"
3. Run build command `bitbake nano`